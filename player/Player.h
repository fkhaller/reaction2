// CryEngine Header File.
// Copyright (C), Crytek, 1999-2015.

#pragma once

class CPlayer : public CGameObjectExtensionHelper<CPlayer, ISimpleExtension>
{
public:
	CPlayer();
	virtual ~CPlayer();

	//ISimpleExtension
	virtual bool Init(IGameObject* pGameObject) override;
	virtual void PostInit(IGameObject* pGameObject) override;
	virtual void Update(SEntityUpdateContext& ctx, int updateSlot) override;
	virtual void HandleEvent(const SGameObjectEvent& event) override;
	virtual void Release() override;
	//~ISimpleExtension

private:
	void SetVars(char* dinosaur);
	void InitEvents();

	std::vector<string> m_extensions;
	PlayerVar g_playerVar;

	SGameObjectEvent* m_die;
	SGameObjectEvent* m_recover;
	SGameObjectEvent* m_recovered;

	int* m_pEvents;

	bool m_bRecovering;

	int m_health;
	int m_hunger;
	int m_thirst;
	int m_stamina;
};
