// CryEngine Source File.
// Copyright (C), Crytek, 1999-2015.


#include "StdAfx.h"
#include "Player.h"
#include <Serialization/IArchiveHost.h>

#define NUMBER_OF_EVENTS END_OF_EVENTS / 2

CPlayer::CPlayer() 
	: m_bRecovering(false)
{
}

CPlayer::~CPlayer()
{
}

bool CPlayer::Init(IGameObject* pGameObject)
{
	SetGameObject(pGameObject);

	SetVars("player");
	void* ptr = &g_playerVar;
	pGameObject->SetUserData(ptr);

	m_hunger = g_playerVar.maxHunger;
	m_thirst = g_playerVar.maxThirst;
	m_stamina = g_playerVar.maxStamina;

	return pGameObject->BindToNetwork();
}

void CPlayer::PostInit(IGameObject* pGameObject)
{
	m_extensions.push_back("ViewExtension");
	m_extensions.push_back("InputExtension");
	m_extensions.push_back("MovementExtension");
	m_extensions.push_back("MannequinExtension");

	m_die = new SGameObjectEvent(DIE, eGOEF_ToExtensions);
	m_recover = new SGameObjectEvent(RECOVERING_STAMINA, eGOEF_ToExtensions);
	m_recovered = new SGameObjectEvent(RECOVERED_STAMINA, eGOEF_ToExtensions);

	InitEvents();

	pGameObject->EnableUpdateSlot(this, 0);
}

void CPlayer::SetVars(char* player){
	//CRY_ASSERT(m_dinosaurVariables != NULL);
	char path[30];

	strcpy(path, player);
	strcat(path, ".json");
	Serialization::LoadJsonFile(g_playerVar, path);
}

//for now all player events will be registered here
//in order to give flexibility for any extention to recieve event
//later this will be more constricted to just the extentions that 
//use the events but right ow this is uncertain.
void CPlayer::InitEvents(){

	m_pEvents = new int[NUMBER_OF_EVENTS];

	int j = 0;
	for (int i = 1; i < END_OF_EVENTS; i <<= 1){
		m_pEvents[j++] = i;
	}

	for (auto& extension : m_extensions)
	{
		GetGameObject()->RegisterExtForEvents(
			GetGameObject()->AcquireExtension(extension.c_str()), m_pEvents, NUMBER_OF_EVENTS);
	}
	GetGameObject()->RegisterExtForEvents(this, m_pEvents, NUMBER_OF_EVENTS);
}


void CPlayer::HandleEvent(const SGameObjectEvent& event){
	switch (event.event){
	case SPRINTING:
		m_stamina -= 2;
		break;
	case CHARACTER_EAT:
		m_hunger += 1000;
		m_thirst += 1000;
		break;
	}
}

void CPlayer::Update(SEntityUpdateContext& ctx, int updateSlot){
	m_hunger -= 1;
	m_thirst -= 1;
	m_stamina += 1;
	if (m_hunger == 0 || m_thirst == 0){
		GetGameObject()->SendEvent(*m_die);
		GetEntity()->Hide(true);
	}
	if (m_stamina <= 0 && !m_bRecovering){
		GetGameObject()->SendEvent(*m_recover);
		CryLog("Stopped Sprinting");
		m_bRecovering = true;
	}
	else if (m_stamina == g_playerVar.maxStamina / 4 && m_bRecovering){
		GetGameObject()->SendEvent(*m_recovered);
		m_bRecovering = false;
	}
}

void CPlayer::Release()
{
	for (auto& extension : m_extensions)
	{
		GetGameObject()->ReleaseExtension(extension.c_str());
	}

	ISimpleExtension::Release();
}
