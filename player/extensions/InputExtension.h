// CryEngine Header File.
// Copyright (C), Crytek, 1999-2015.


#pragma once

#include <ICryMannequin.h>
#include "MannequinExtension.h"

class CInputExtension : public CGameObjectExtensionHelper<CInputExtension, ISimpleExtension>, IActionListener, ISystemEventListener
{
public:
	// ISimpleExtension
	virtual void PostInit(IGameObject* pGameObject) override;
	virtual void FullSerialize(TSerialize ser) override;
	virtual void Update(SEntityUpdateContext& ctx, int updateSlot) override;
	virtual void HandleEvent(const SGameObjectEvent& event) override;
	virtual void Release() override;
	// ~ISimpleExtension

	// IActionListener
	virtual void OnAction(const ActionId& action, int activationMode, float value) override;
	// ~IActionListener

	// ISystemEventListener
	void OnSystemEvent(ESystemEvent event, UINT_PTR wparam, UINT_PTR lparam) override;
	// ~ISystemEventListener

	CInputExtension();
	virtual ~CInputExtension();

private:
	bool OnActionMoveForward(EntityId entityId, const ActionId& actionId, int activationMode, float value);
	bool OnActionMoveBackward(EntityId entityId, const ActionId& actionId, int activationMode, float value);
	bool OnActionMoveLeft(EntityId entityId, const ActionId& actionId, int activationMode, float value);
	bool OnActionMoveRight(EntityId entityId, const ActionId& actionId, int activationMode, float value);
	bool OnActionMoveUp(EntityId entityId, const ActionId& actionId, int activationMode, float value);
	bool OnActionMoveDown(EntityId entityId, const ActionId& actionId, int activationMode, float value);
	bool OnActionSpinLeft(EntityId entityId, const ActionId& actionId, int activationMode, float value);
	bool OnActionSpinRight(EntityId entityId, const ActionId& actionId, int activationMode, float value);
	bool OnActionMouseRotateYaw(EntityId entityId, const ActionId& actionId, int activationMode, float value);
	bool OnActionMouseRotatePitch(EntityId entityId, const ActionId& actionId, int activationMode, float value);
	bool OnActionJump(EntityId entityId, const ActionId& actionId, int activationMode, float value);
	bool OnActionSprint(EntityId entityId, const ActionId& actionId, int activationMode, float value);
	bool OnActionZoomIn(EntityId entityId, const ActionId& actionId, int activationMode, float value);
	bool OnActionZoomOut(EntityId entityId, const ActionId& actionId, int activationMode, float value);
	bool OnActionUse(EntityId entityId, const ActionId& actionId, int activationMode, float value);

	void RegisterInputHandler();
	void InitGameObjectEvents();

	IEntity* m_pLookAtEntity;
	
	TActionHandler<CInputExtension> m_playerActionHandler;
	float m_mouseSensitivity;

	float m_mouseYawValue;
	void* m_pMouseYawValue;
	float m_mousePitchValue;
	void* m_pMousePitchValue;

	bool m_bSprint;
	bool m_bCanSprint;

	SGameObjectEvent* m_rotateCameraYaw;
	SGameObjectEvent* m_rotateCameraPitch;
	SGameObjectEvent* m_rotateCharLeft;
	SGameObjectEvent* m_rotateCharRight;
	SGameObjectEvent* m_moveCharForward;
	SGameObjectEvent* m_moveCharLeft;
	SGameObjectEvent* m_moveCharRight;
	SGameObjectEvent* m_moveCharBackward;
	SGameObjectEvent* m_sprint;
	SGameObjectEvent* m_jump;
	SGameObjectEvent* m_moveForwardStop;
	SGameObjectEvent* m_moveBackwardStop;
	SGameObjectEvent* m_moveLeftStop;
	SGameObjectEvent* m_moveRightStop;
	SGameObjectEvent* m_charIdle;
	SGameObjectEvent* m_cameraZoomIn;
	SGameObjectEvent* m_cameraZoomOut;
	SGameObjectEvent* m_eat;
	SGameObjectEvent* m_drink;

	PlayerVar g_playerVar;
};
