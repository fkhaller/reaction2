// CryEngine Source File.
// Copyright (C), Crytek, 1999-2015.


#include "StdAfx.h"
#include "MovementExtension.h"
#include "physinterface.h"
#include "IViewSystem.h"

#define JUMP_HEIGHT 10.0


CMovementExtension::CMovementExtension()
	: m_rotation(ZERO)
	, m_vDeltaMovement(0.0f)
	, m_movementSpeed(0.0f)
	, m_bInAir(true)
	, m_bJump(false)
	, m_fJumpCounter(0.0)
	, m_fJumpSpeed(1.0)
{
}

CMovementExtension::~CMovementExtension()
{
}

void CMovementExtension::Release()
{
	ISimpleExtension::Release();
}

void CMovementExtension::PostInit(IGameObject* pGameObject)
{
	pGameObject->EnablePostUpdates(this);

	g_playerVar = *(PlayerVar*)pGameObject->GetUserData();

	GetEntity()->SetPos(Vec3(230,160,10));
}

void CMovementExtension::HandleEvent(const SGameObjectEvent& event){
	switch (event.event){
		case ROTATE_CHARACTER_LEFT:
		case ROTATE_CHARACTER_RIGHT:
			m_rotation.z -= g_playerVar.spinSpeed * *(float*)event.param / MAX_MOUSE_INPUT;
			break;
		case MOVE_CHARACTER_FORWARD:
			m_vDeltaMovement.y = g_playerVar.forwardSpeed;
			break;
		case MOVE_CHARACTER_BACKWARD:
			m_vDeltaMovement.y = -g_playerVar.backwardSpeed;
			break;
		case MOVE_CHARACTER_LEFT:
			m_vDeltaMovement.x = -g_playerVar.strafeSpeed;
			break;
		case MOVE_CHARACTER_RIGHT:
			m_vDeltaMovement.x = g_playerVar.strafeSpeed;
			break;
		case MOVE_FORWARD_STOP:
		case MOVE_BACKWARD_STOP:
			if (m_vDeltaMovement.y != 0)
				m_vDeltaMovement.y = 0;
			break;
		case MOVE_LEFT_STOP:
		case MOVE_RIGHT_STOP:
			if (m_vDeltaMovement.x != 0)
				m_vDeltaMovement.x = 0;
			break;
		case MOVE_JUMP:
			if (m_bInAir == false){
				m_bJump = true;
			}
			break;
	}
}

void CMovementExtension::PostUpdate(float frameTime)
{
	ray_hit pHit;
	pHit.pCollider = NULL;
	const SViewParams* params = gEnv->pGame->GetIGameFramework()->GetIViewSystem()->GetActiveView()->GetCurrentParams();
	Vec3 vRotation = Vec3(0,0,-1);

	int hits = gEnv->pPhysicalWorld->RayWorldIntersection(
	params->position, vRotation * 2, ent_all, rwi_any_hit, &pHit, 1);

	if (m_bJump){
		m_vDeltaMovement.z = m_fJumpSpeed;
		m_fJumpCounter += m_fJumpSpeed;
		if (m_fJumpSpeed > .2)
			m_fJumpSpeed -= .05;
	}
	else if(!hits){
		m_vDeltaMovement.z -= .03;
		m_bInAir = true;
	}
	else{
		m_vDeltaMovement.z = 0;
		m_bInAir = false;
	}

	


	SmartScriptTable inputParams(gEnv->pScriptSystem);
	if (GetGameObject()->GetExtensionParams("InputExtension", inputParams))
	{
		Quat rotation(m_rotation);		
		GetEntity()->SetRotation(rotation.GetNormalized());

		bool bSprint = false;
		inputParams->GetValue("sprint", bSprint);

		const float sprintMultiplier = bSprint ? g_playerVar.sprintMultiplier : 1.0f;

		GetEntity()->SetPos(GetEntity()->GetWorldPos() + rotation * (m_vDeltaMovement * 10 * frameTime * sprintMultiplier));
	}

	if (m_fJumpCounter > JUMP_HEIGHT){
		m_bJump = false;
		m_vDeltaMovement.z = 0;
		m_fJumpCounter = 0;
		m_fJumpSpeed = 1.0;
	}
}

void CMovementExtension::FullSerialize(TSerialize ser)
{
	if (ser.GetSerializationTarget() == eST_Script)
	{
		ser.Value("rotation", m_rotation);
		ser.Value("deltaMovement", m_vDeltaMovement);
	}
}
