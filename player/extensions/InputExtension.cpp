// CryEngine Source File.
// Copyright (C), Crytek, 1999-2015.


#include "StdAfx.h"
#include "InputExtension.h"
#include "physinterface.h"
#include "IViewSystem.h"

CInputExtension::CInputExtension()
	: m_mouseSensitivity(0.0f)
	, m_bSprint(false)
	, m_bCanSprint(true)
	, m_pLookAtEntity(NULL)
{
}

CInputExtension::~CInputExtension()
{
}

void CInputExtension::Release()
{
	GetGameObject()->ReleaseActions(this);

	if (gEnv->IsEditor())
	{
		gEnv->pSystem->GetISystemEventDispatcher()->RemoveListener(this);
	}
	
	gEnv->pConsole->UnregisterVariable("gamezero_mouse_sensitivity", true);

	ISimpleExtension::Release();
}

void CInputExtension::PostInit(IGameObject* pGameObject)
{
	RegisterInputHandler();

	m_pMousePitchValue = &m_mousePitchValue;
	m_pMouseYawValue = &m_mouseYawValue;

	InitGameObjectEvents();

	pGameObject->EnableUpdateSlot(this, 0);

	IActionMapManager* pActionMapManager = gEnv->pGame->GetIGameFramework()->GetIActionMapManager();
	if (pActionMapManager)
	{
		pActionMapManager->InitActionMaps("libs/config/defaultprofile.xml");
		pActionMapManager->SetDefaultActionEntity(GetEntityId());
		pActionMapManager->EnableActionMap("player", true);
		pActionMapManager->Enable(true);
	}

	pGameObject->CaptureActions(this);

	if (gEnv->IsEditor())
	{
		gEnv->pSystem->GetISystemEventDispatcher()->RegisterListener(this);
	}

	g_playerVar = *(PlayerVar*)pGameObject->GetUserData();

	REGISTER_CVAR2("gamezero_mouse_sensitivity", &m_mouseSensitivity, 0.002f, VF_NULL, "Mouse Sensitivity.");
}

void CInputExtension::HandleEvent(const SGameObjectEvent& event){
	switch (event.event){
		case RECOVERING_STAMINA:
			m_bCanSprint = false;
			break;
		case RECOVERED_STAMINA:
			m_bCanSprint = true;
			break;
	}
}

void CInputExtension::Update(SEntityUpdateContext& ctx, int updateSlot){

	if (m_bSprint && m_bCanSprint){
		GetGameObject()->SendEvent(*m_sprint);
	}

	//get all of the entities
	/*f32 length = _I32_MAX;
	Vec3 direction = FORWARD_DIRECTION;
	IEntityIt* itt = gEnv->pEntitySystem->GetEntityIterator();
	IEntity* current;

	do{
		current = itt->Next();
		f32 disFromChar = (current->GetPos() - GetEntity()->GetPos()).GetLength();
		Vec3 dirFromChar = (current->GetPos() - GetEntity()->GetPos()).GetNormalized();

		if ((strcmp(current->GetName(), "water") == 0 || strcmp(current->GetName(), "plant") == 0 || strcmp(current->GetName(), "carcous") == 0)
			&& disFromChar < 10 && disFromChar < length && (dirFromChar + GetEntity()->GetForwardDir()).GetLength() > dirFromChar.GetLength() + .95){
			length = disFromChar;
			direction = dirFromChar;
			m_pLookAtEntity = current;
		}
	} while (!itt->IsEnd());

	if (length > 10 || (direction + GetEntity()->GetForwardDir()).GetLength() < direction.GetLength() + .95){
		m_pLookAtEntity = NULL;
	}*/	
}

void CInputExtension::RegisterInputHandler()
{
	m_playerActionHandler.AddHandler(ActionId("mouse_rotateyaw"),		&CInputExtension::OnActionMouseRotateYaw);
	m_playerActionHandler.AddHandler(ActionId("mouse_rotatepitch"),	&CInputExtension::OnActionMouseRotatePitch);

	m_playerActionHandler.AddHandler(ActionId("moveforward"),	&CInputExtension::OnActionMoveForward);
	m_playerActionHandler.AddHandler(ActionId("moveback"),		&CInputExtension::OnActionMoveBackward);
	m_playerActionHandler.AddHandler(ActionId("moveleft"),		&CInputExtension::OnActionMoveLeft);
	m_playerActionHandler.AddHandler(ActionId("moveright"),		&CInputExtension::OnActionMoveRight);
	m_playerActionHandler.AddHandler(ActionId("spinleft"), &CInputExtension::OnActionSpinLeft);
	m_playerActionHandler.AddHandler(ActionId("spinright"), &CInputExtension::OnActionSpinRight);

	m_playerActionHandler.AddHandler(ActionId("zoomin"), &CInputExtension::OnActionZoomIn);
	m_playerActionHandler.AddHandler(ActionId("zoomout"), &CInputExtension::OnActionZoomOut);
	m_playerActionHandler.AddHandler(ActionId("use"), &CInputExtension::OnActionUse);

	m_playerActionHandler.AddHandler(ActionId("sprint"),				&CInputExtension::OnActionSprint);
	m_playerActionHandler.AddHandler(ActionId("jump"), &CInputExtension::OnActionJump);
}

void CInputExtension::InitGameObjectEvents(){

	m_sprint = new SGameObjectEvent(SPRINTING, eGOEF_ToExtensions);
	m_rotateCameraYaw = new SGameObjectEvent(ROTATE_CAMERA_YAW, eGOEF_ToExtensions, (IGameObjectSystem::ExtensionID)65535U, m_pMouseYawValue);
	m_rotateCameraPitch = new SGameObjectEvent(ROTATE_CAMERA_PITCH, eGOEF_ToExtensions, (IGameObjectSystem::ExtensionID)65535U, m_pMousePitchValue);
	m_rotateCharLeft = new SGameObjectEvent(ROTATE_CHARACTER_LEFT, eGOEF_ToExtensions, (IGameObjectSystem::ExtensionID)65535U, m_pMouseYawValue);
	m_rotateCharRight = new SGameObjectEvent(ROTATE_CHARACTER_RIGHT, eGOEF_ToExtensions, (IGameObjectSystem::ExtensionID)65535U, m_pMouseYawValue);
	m_moveCharForward = new SGameObjectEvent(MOVE_CHARACTER_FORWARD, eGOEF_ToExtensions);
	m_moveCharLeft = new SGameObjectEvent(MOVE_CHARACTER_LEFT, eGOEF_ToExtensions);
	m_moveCharRight = new SGameObjectEvent(MOVE_CHARACTER_RIGHT, eGOEF_ToExtensions);
	m_moveCharBackward = new SGameObjectEvent(MOVE_CHARACTER_BACKWARD, eGOEF_ToExtensions);
	m_jump = new SGameObjectEvent(MOVE_JUMP, eGOEF_ToExtensions);
	m_moveForwardStop = new SGameObjectEvent(MOVE_FORWARD_STOP, eGOEF_ToExtensions);
	m_moveBackwardStop = new SGameObjectEvent(MOVE_BACKWARD_STOP, eGOEF_ToExtensions);
	m_moveLeftStop = new SGameObjectEvent(MOVE_LEFT_STOP, eGOEF_ToExtensions);
	m_moveRightStop = new SGameObjectEvent(MOVE_RIGHT_STOP, eGOEF_ToExtensions);
	m_charIdle = new SGameObjectEvent(CHARACTER_IDLE, eGOEF_ToExtensions);
	m_cameraZoomIn = new SGameObjectEvent(CAMERA_ZOOM_IN, eGOEF_ToExtensions);
	m_cameraZoomOut = new SGameObjectEvent(CAMERA_ZOOM_OUT, eGOEF_ToExtensions);
	m_eat = new SGameObjectEvent(CHARACTER_EAT, eGOEF_ToExtensions);
	m_drink = new SGameObjectEvent(CHARACTER_DRINK, eGOEF_ToExtensions);

}

void CInputExtension::OnAction(const ActionId& action, int activationMode, float value)
{	
	m_playerActionHandler.Dispatch(this, GetEntityId(), action, activationMode, value);
}

bool CInputExtension::OnActionMoveForward(EntityId entityId, const ActionId& actionId, int activationMode, float value)
{
	if (activationMode == eAAM_OnPress || activationMode == eAAM_OnHold){
		GetGameObject()->SendEvent(*m_moveCharForward);
	}
	else if (activationMode == eAAM_OnRelease){
		GetGameObject()->SendEvent(*m_moveForwardStop);
	}

	return false;
}

bool CInputExtension::OnActionMoveBackward(EntityId entityId, const ActionId& actionId, int activationMode, float value)
{
	if (activationMode == eAAM_OnPress || activationMode == eAAM_OnHold){
		GetGameObject()->SendEvent(*m_moveCharBackward);
	}
	else if (activationMode == eAAM_OnRelease){
		GetGameObject()->SendEvent(*m_moveBackwardStop);
	}

	return false;
}

bool CInputExtension::OnActionMoveLeft(EntityId entityId, const ActionId& actionId, int activationMode, float value)
{
	if (activationMode == eAAM_OnPress || activationMode == eAAM_OnHold){
		GetGameObject()->SendEvent(*m_moveCharLeft);
	}
	else if (activationMode == eAAM_OnRelease){
		GetGameObject()->SendEvent(*m_moveLeftStop);
	}

	return false;
}

bool CInputExtension::OnActionMoveRight(EntityId entityId, const ActionId& actionId, int activationMode, float value)
{
	if (activationMode == eAAM_OnPress || activationMode == eAAM_OnHold){
		GetGameObject()->SendEvent(*m_moveCharRight);
	}
	else if (activationMode == eAAM_OnRelease){
		GetGameObject()->SendEvent(*m_moveRightStop);
	}

	return false;
}

bool CInputExtension::OnActionJump(EntityId entityId, const ActionId& actionId, int activationMode, float value)
{
	if (activationMode == eAAM_OnPress){
		GetGameObject()->SendEvent(*m_jump);
	}

	return false;
}

bool CInputExtension::OnActionSpinLeft(EntityId entityId, const ActionId& actionId, int activationMode, float value){

	if (activationMode == eAAM_OnPress || activationMode == eAAM_OnHold){
		GetGameObject()->SendEvent(*m_rotateCharLeft);
	}

	return false;
}

bool CInputExtension::OnActionSpinRight(EntityId entityId, const ActionId& actionId, int activationMode, float value){

	if (activationMode == eAAM_OnPress || activationMode == eAAM_OnHold){
		GetGameObject()->SendEvent(*m_rotateCharRight);
	}

	return false;
}

bool CInputExtension::OnActionMouseRotateYaw(EntityId entityId, const ActionId& actionId, int activationMode, float value)
{
	if (value != 0){
		m_mouseYawValue = value;
		if (value < 0){
			GetGameObject()->SendEvent(*m_rotateCharLeft);
		}
		else{
			GetGameObject()->SendEvent(*m_rotateCharRight);
		}
	}
	 
	return false;
}

bool CInputExtension::OnActionMouseRotatePitch(EntityId entityId, const ActionId& actionId, int activationMode, float value)
{
	if (value != 0){
		m_mousePitchValue = value;
		GetGameObject()->SendEvent(*m_rotateCameraPitch);
	}

	return false;
}

bool CInputExtension::OnActionSprint(EntityId entityId, const ActionId& actionId, int activationMode, float value)
{
	if (m_bCanSprint){
		if (activationMode == eAAM_OnPress)
			m_bSprint = true;
		else if (activationMode == eAAM_OnRelease)
			m_bSprint = false;
	}
	else{
		m_bSprint = false;
	}

	return false;
}

bool CInputExtension::OnActionZoomIn(EntityId entityId, const ActionId& actionId, int activationMode, float value){
	GetGameObject()->SendEvent(*m_cameraZoomIn);
	return false;
}

bool CInputExtension::OnActionZoomOut(EntityId entityId, const ActionId& actionId, int activationMode, float value){
	GetGameObject()->SendEvent(*m_cameraZoomOut);
	return false;
}

bool CInputExtension::OnActionUse(EntityId entityId, const ActionId& actionId, int activationMode, float value){

	if (activationMode == eAAM_OnPress){
		if (m_pLookAtEntity != NULL){
			const char* name = m_pLookAtEntity->GetName();

			if (strncmp(name, "water", 5) == 0){
				GetGameObject()->SendEvent(*m_drink);
			}
			else if (strncmp(name, "plant", 5) == 0 && g_playerVar.type == 0){
				GetGameObject()->SendEvent(*m_eat);
				m_pLookAtEntity->Hide(true);
			}
			else if (strncmp(name, "carcous", 5) == 0 && g_playerVar.type == 1){
				GetGameObject()->SendEvent(*m_eat);
				m_pLookAtEntity->Hide(true);
			}
		}
	}

	return false;
}

void CInputExtension::FullSerialize(TSerialize ser)
{
	if (ser.GetSerializationTarget() == eST_Script)
	{
		ser.Value("sprint", m_bSprint);
	}
}

void CInputExtension::OnSystemEvent(ESystemEvent event, UINT_PTR wparam, UINT_PTR lparam)
{
	switch (event)
	{
	case ESYSTEM_EVENT_EDITOR_GAME_MODE_CHANGED:
		{
			//m_rotation = GetEntity()->GetWorldAngles();
		}
		break;
	}
}
