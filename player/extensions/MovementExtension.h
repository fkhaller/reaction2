// CryEngine Header File.
// Copyright (C), Crytek, 1999-2015.


#pragma once
#include "IMovementController.h"

class CMovementExtension : public CGameObjectExtensionHelper<CMovementExtension, ISimpleExtension>
{
public:
	// ISimpleExtension
	virtual void PostInit(IGameObject* pGameObject) override;
	virtual void PostUpdate(float frameTime) override;
	virtual void HandleEvent(const SGameObjectEvent& event) override;
	virtual void FullSerialize(TSerialize ser) override;
	virtual void Release() override;
	// ~ISimpleExtension
	
	CMovementExtension();
	virtual ~CMovementExtension();

private:
	float m_movementSpeed;
	Vec3 m_vDeltaMovement;
	Ang3 m_rotation;

	bool m_bInAir;
	bool m_bJump;

	float m_fJumpCounter;
	float m_fJumpSpeed;

	PlayerVar g_playerVar;
};
