// CryEngine Source File.
// Copyright (C), Crytek, 1999-2015.


#include "StdAfx.h"
#include "ViewExtension.h"
#include <IViewSystem.h>
#include "InputExtension.h"


CViewExtension::CViewExtension()
	: m_viewId(0)
	, m_camFOV(0.0f)
{
}

CViewExtension::~CViewExtension()
{
}

void CViewExtension::Release()
{
	GetGameObject()->ReleaseView(this);

	gEnv->pGame->GetIGameFramework()->GetIViewSystem()->RemoveView(m_viewId);

	ISimpleExtension::Release();
}

void CViewExtension::PostInit(IGameObject* pGameObject)
{
	CreateView();
	pGameObject->CaptureView(this);

	g_playerVar = *(PlayerVar*)pGameObject->GetUserData();

	m_camFOV = g_playerVar.fov;
}

void CViewExtension::CreateView()
{
	IViewSystem* pViewSystem = gEnv->pGame->GetIGameFramework()->GetIViewSystem();
	IView* pView = pViewSystem->CreateView();

	pView->LinkTo(GetGameObject());
	pViewSystem->SetActiveView(pView);

	m_viewId = pViewSystem->GetViewId(pView);
}

void CViewExtension::HandleEvent(const SGameObjectEvent& event){

	SmartScriptTable inputParams(gEnv->pScriptSystem);
	if (GetGameObject()->GetExtensionParams("MovementExtension", inputParams))
	{
		Ang3 rotation(ZERO);
		inputParams->GetValue("rotation", rotation);

		switch (event.event){
			case ROTATE_CAMERA_YAW:
				m_cameraYaw -= g_playerVar.headSpeed * *(float*)event.param / MAX_MOUSE_INPUT;
				m_cameraYaw = clamp_tpl(m_cameraYaw, rotation.z - g_playerVar.yawLimit, rotation.z + g_playerVar.yawLimit);
				break;
			case ROTATE_CAMERA_PITCH:
				m_cameraPitch -= g_playerVar.headSpeed * *(float*)event.param / MAX_MOUSE_INPUT;
				m_cameraPitch = clamp_tpl(m_cameraPitch, -g_playerVar.pitchMin, g_playerVar.pitchMax);
				break;
			case CAMERA_ZOOM_IN:
				if (m_camFOV > 10){
					m_camFOV -= 10;
				}
				break;
			case CAMERA_ZOOM_OUT:
				if (m_camFOV < g_playerVar.fov){
					m_camFOV += 10;
				}
				break;
		}
	}

}

void CViewExtension::UpdateView(SViewParams& params)
{
	SmartScriptTable inputParams(gEnv->pScriptSystem);
	if (GetGameObject()->GetExtensionParams("MovementExtension", inputParams))
	{
		Ang3 rotation(ZERO);
		inputParams->GetValue("rotation", rotation);

		params.position = GetEntity()->GetWorldPos()
			+ GetEntity()->GetForwardDir().GetRotated(Vec3(0, 0, 1), gf_PI) * g_playerVar.cameraOffsetX
			+ Vec3(0, 0, g_playerVar.cameraOffsetZ);

		params.rotation.SetRotationXYZ(Ang3(m_cameraPitch, 0, rotation.z));

		params.fov = DEG2RAD(m_camFOV);
	}
}
