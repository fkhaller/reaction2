#include "StdAfx.h"
#include <ICryMannequin.h>
#include "MannequinExtension.h"



CMannequinExtension::CMannequinExtension() 
	: m_forwardAnimation(NULL)
	, m_backwardAnimation(NULL)
	, m_strafeLeftAnimation(NULL)
	, m_strafeRightAnimation(NULL)
	, m_spinLeftAnimation(NULL)
	, m_spinRightAnimation(NULL)
	, m_sprintAnimation(NULL)
	, m_currentEvent(0)
{
}


CMannequinExtension::~CMannequinExtension()
{
}

void CMannequinExtension::Release(){
	ISimpleExtension::Release();
}

void CMannequinExtension::PostInit(IGameObject* pGameObject){
	//should be parameterized to trex
	m_pCharacter = gEnv->pCharacterManager->CreateInstance("Objects/characters/human/sdk_player/sdk_player.cdf");
	CRY_ASSERT(m_pCharacter == NULL);
	GetEntity()->SetCharacter(m_pCharacter, 0);

	SetupMannequin();
	SetupAnimations();

	pGameObject->EnableUpdateSlot(this, 0);
}

void CMannequinExtension::HandleEvent(const SGameObjectEvent& event){

	switch (event.event){
		case DIE:
			break;
		case MOVE_CHARACTER_FORWARD:
			if (m_currentEvent != MOVE_CHARACTER_FORWARD && m_currentEvent != MOVE_FORWARD_LEFT && m_currentEvent != MOVE_FORWARD_RIGHT){
				m_pActionController->Flush();
				if (m_currentEvent == MOVE_CHARACTER_LEFT){ 
					m_currentEvent = MOVE_FORWARD_LEFT; 
					MoveForwardLeft();
				}
				else if (m_currentEvent == MOVE_CHARACTER_RIGHT){ 
					m_currentEvent = MOVE_FORWARD_RIGHT; 
					MoveForwardRight();
				}
				else{
					m_currentEvent = MOVE_CHARACTER_FORWARD;
					MoveForward();
				}
			}
			break;
		case MOVE_CHARACTER_BACKWARD:
			if (!(m_currentEvent == MOVE_CHARACTER_BACKWARD)){
				m_currentEvent = MOVE_CHARACTER_BACKWARD;
				m_pActionController->Flush();
				MoveBackward();
			}
			break;
		case MOVE_CHARACTER_LEFT:
			if (m_currentEvent != MOVE_CHARACTER_LEFT && m_currentEvent != MOVE_FORWARD_LEFT && m_currentEvent != MOVE_FORWARD_RIGHT){
				m_pActionController->Flush();
				if (m_currentEvent == MOVE_CHARACTER_FORWARD){
					m_currentEvent = MOVE_FORWARD_LEFT;
					MoveForwardLeft();
				}
				else{
					m_currentEvent = MOVE_CHARACTER_LEFT;
					MoveLeft();
				}
			}
			break;
		case MOVE_CHARACTER_RIGHT:
			if (m_currentEvent != MOVE_CHARACTER_RIGHT && m_currentEvent != MOVE_FORWARD_LEFT && m_currentEvent != MOVE_FORWARD_RIGHT){
				m_pActionController->Flush();
				if (m_currentEvent == MOVE_CHARACTER_FORWARD){
					m_currentEvent = MOVE_FORWARD_RIGHT;
					MoveForwardRight();
				}
				else{
					m_currentEvent = MOVE_CHARACTER_RIGHT;
					MoveRight();
				}
			}
			break;
			//rotation animations are not necessary to start can be added later 
	//	case ROTATE_CHARACTER_LEFT:
	//		if (!(m_currentEvent == ROTATE_CHARACTER_LEFT)){
	//			m_currentEvent = ROTATE_CHARACTER_LEFT;
	//			SpinLeft();
	//		}
	//		break;
	//	case ROTATE_CHARACTER_RIGHT:
	//		if (!(m_currentEvent == ROTATE_CHARACTER_RIGHT)){
	//			m_currentEvent = ROTATE_CHARACTER_RIGHT;
	//			SpinRight();
	//		}
			break;
		case MOVE_FORWARD_STOP:
			m_pActionController->Flush();
			if (m_currentEvent == MOVE_FORWARD_LEFT){
				m_currentEvent = MOVE_CHARACTER_LEFT;
				MoveLeft();
			}
			else if (m_currentEvent == MOVE_FORWARD_RIGHT){
				m_currentEvent = MOVE_CHARACTER_RIGHT;
				MoveRight();
			}
			else if(m_currentEvent != CHARACTER_IDLE){
				m_currentEvent = CHARACTER_IDLE;
				Idle();
			}
			break;
		case MOVE_LEFT_STOP:
			m_pActionController->Flush();
			if (m_currentEvent == MOVE_FORWARD_LEFT){
				m_currentEvent = MOVE_CHARACTER_FORWARD;
				MoveForward();
			}
			else if (m_currentEvent != CHARACTER_IDLE){
				m_currentEvent = CHARACTER_IDLE;
				Idle();
			}
			break;
		case MOVE_RIGHT_STOP:
			m_pActionController->Flush();
			if (m_currentEvent == MOVE_FORWARD_RIGHT){
				m_currentEvent = MOVE_CHARACTER_FORWARD;
				MoveForward();
			}
			else if (m_currentEvent != CHARACTER_IDLE){
				m_currentEvent = CHARACTER_IDLE;
				Idle();
			}
			break;
		case MOVE_BACKWARD_STOP:
		case CHARACTER_IDLE:
			if (m_currentEvent != CHARACTER_IDLE){
				m_currentEvent = CHARACTER_IDLE;
				m_pActionController->Flush();
				Idle();
			}
			break;
		case CHARACTER_EAT:
			CryLog("eating");
			break;
	}
}

void CMannequinExtension::Update(SEntityUpdateContext& ctx, int updateSlot){

	//CRYENGINE Documentation
	if (m_pActionController != NULL)
	{
		m_pActionController->Update(gEnv->pTimer->GetFrameTime());
	}
}

void CMannequinExtension::MoveForward(){
	SmartScriptTable inputParams(gEnv->pScriptSystem);
	if (GetGameObject()->GetExtensionParams("InputExtension", inputParams))
	{
		bool bSprint = false;
		inputParams->GetValue("sprint", bSprint);
		if (!bSprint){
			if (m_forwardAnimation != NULL){
				m_pActionController->Queue(*m_forwardAnimation);
			}
		}
		else{
			if (m_sprintAnimation != NULL){
				m_pActionController->Queue(*m_sprintAnimation);
			}
		}
	}
}

void CMannequinExtension::MoveBackward(){
	if (m_backwardAnimation != NULL){
		m_pActionController->Queue(*m_backwardAnimation);
	}
}

void CMannequinExtension::MoveRight(){
	if (m_strafeRightAnimation != NULL){
		m_pActionController->Queue(*m_strafeRightAnimation);
	}
}

void CMannequinExtension::MoveLeft(){
	if (m_strafeLeftAnimation != NULL){
		m_pActionController->Queue(*m_strafeLeftAnimation);
	}
}

void CMannequinExtension::MoveForwardRight(){
	if (m_forwardRightAnimation != NULL){
		m_pActionController->Queue(*m_forwardRightAnimation);
	}
}

void CMannequinExtension::MoveForwardLeft(){
	if (m_forwardRightAnimation != NULL){
		m_pActionController->Queue(*m_forwardLeftAnimation);
	}
}

void CMannequinExtension::SpinRight(){
	if (m_spinRightAnimation != NULL){
		m_pActionController->Queue(*m_spinRightAnimation);
	}
}

void CMannequinExtension::SpinLeft(){
	if (m_spinLeftAnimation != NULL){
		m_pActionController->Queue(*m_spinLeftAnimation);
	}
}

void CMannequinExtension::Idle(){
	if (m_idle != NULL){
		m_pActionController->Queue(*m_idle);
	}
}

// Mannequin Initialization (This code thanks to CRYENGINE Documentation)
void CMannequinExtension::SetupMannequin(){

	IMannequin& mannequinInterface = gEnv->pGame->GetIGameFramework()->GetMannequinInterface();
	IAnimationDatabaseManager& animationDatabaseManager = mannequinInterface.GetAnimationDatabaseManager();

	// Loading the controller definition 
	const SControllerDef* const pControllerDef = animationDatabaseManager.LoadControllerDef("Animations/Mannequin/ADB/humanControllerDefs.xml");
	if (pControllerDef == NULL)
	{
		CryWarning(VALIDATOR_MODULE_GAME, VALIDATOR_ERROR, "Failed to load humanControllerDefs.");
		return;
	}

	// Creation of the animation context.
	CRY_ASSERT(m_pAnimationContext == NULL);
	m_pAnimationContext = new SAnimationContext(*pControllerDef);

	// Creation of the action controller
	CRY_ASSERT(m_pActionController == NULL);
	m_pActionController = mannequinInterface.CreateActionController(GetEntity(), *m_pAnimationContext);

	// Scope Context Setup.
	const TagID mainCharacterScopeContextId = m_pAnimationContext->controllerDef.m_scopeContexts.Find("Char3P");
	if (mainCharacterScopeContextId == TAG_ID_INVALID)
	{
		CryWarning(VALIDATOR_MODULE_GAME, VALIDATOR_ERROR, "Failed to find Char3P scope context id for playerControllerDefs.");
		return;
	}

	ICharacterInstance* const pCharacterInstance = GetEntity()->GetCharacter(0);
	CRY_ASSERT(pCharacterInstance != NULL);

	// Loading a database
	const IAnimationDatabase* const pAnimationDatabase = animationDatabaseManager.Load("Animations/Mannequin/ADB/human.adb");
	if (pAnimationDatabase == NULL)
	{
		CryWarning(VALIDATOR_MODULE_GAME, VALIDATOR_ERROR, "Failed to load human.");
		return;
	}

	m_pActionController->SetScopeContext(mainCharacterScopeContextId, *GetEntity(), pCharacterInstance, pAnimationDatabase);
}



void CMannequinExtension::SetupAnimations(){

	m_forwardAnimation = getAction("moveForward", 5);
	m_backwardAnimation = getAction("moveBackward", 5);
	m_strafeLeftAnimation = getAction("moveLeft", 5);
	m_strafeRightAnimation = getAction("moveRight", 5);
	m_forwardLeftAnimation = getAction("moveForwardLeft", 5);
	m_forwardRightAnimation = getAction("moveForwardRight", 5);
	m_idle = getAction("idle", 5);
}

IActionPtr CMannequinExtension::getAction(const char* id, int priority){

	const FragmentID fragmentId = m_pAnimationContext->controllerDef.m_fragmentIDs.Find(id);
	CRY_ASSERT(fragmentId == TAG_ID_INVALID);

	IActionPtr pAction = new TAction< SAnimationContext >(priority, fragmentId);
	CRY_ASSERT(pAction == NULL);

	return pAction;
}
