#pragma once

//Manequin should not be an actionlistener playerinput will send events out to this
class CMannequinExtension : public CGameObjectExtensionHelper<CMannequinExtension, ISimpleExtension>
{
public:
	CMannequinExtension();
	~CMannequinExtension();

	//ISimpleExtension
	virtual void PostInit(IGameObject* pGameObject) override;
	virtual void Update(SEntityUpdateContext& ctx, int updateSlot) override;
	virtual void HandleEvent(const SGameObjectEvent& event) override;
	virtual void Release() override;
	//~ISimpleExtension

private:
	void SetupMannequin();
	void SetupAnimations();
	IActionPtr getAction(const char* id, int priority);

	void MoveForward();
	void MoveBackward();
	void MoveRight();
	void MoveLeft();
	void MoveForwardLeft();
	void MoveForwardRight();
	void SpinRight();
	void SpinLeft();
	void Idle();

	ICharacterInstance* m_pCharacter;
	IActionController* m_pActionController;
	SAnimationContext* m_pAnimationContext;

	IActionPtr m_forwardAnimation;
	IActionPtr m_backwardAnimation;
	IActionPtr m_strafeLeftAnimation;
	IActionPtr m_strafeRightAnimation;
	IActionPtr m_forwardLeftAnimation;
	IActionPtr m_forwardRightAnimation;
	IActionPtr m_spinLeftAnimation;
	IActionPtr m_spinRightAnimation;
	IActionPtr m_sprintAnimation;
	IActionPtr m_idle;

	int m_currentEvent;

private:
	enum EAnimationEvents{
		MOVE_FORWARD_RIGHT = 3,
		MOVE_FORWARD_LEFT = 6,
		MOVE_BACKWARD_RIGHT = 9,
		MOVE_BACKWARD_LEFT = 12
	};
};

