// CryEngine Source File.
// Copyright (C), Crytek, 1999-2015.


#pragma once


#include <CryModuleDefs.h>
#define eCryModule eCryM_Game
#define GAME_API DLL_EXPORT

#include <platform.h>
#include <ISystem.h>
#include <I3DEngine.h>
#include <ISerialize.h>
#include "player/extensions/ISimpleExtension.h"
#include "Serialization/IArchive.h"
#include "Serialization/STL.h"

#define MAX_MOUSE_INPUT 635

enum EPlayerEvents{
	DIE = 1 << 0,
	SPRINTING = 1 << 1,
	RECOVERING_STAMINA = 1 << 2,
	RECOVERED_STAMINA = 1 << 3,
	ROTATE_CAMERA_YAW = 1 << 4,
	ROTATE_CAMERA_PITCH = 1 << 5,
	ROTATE_CHARACTER_LEFT = 1 << 6,
	ROTATE_CHARACTER_RIGHT = 1 << 7,
	MOVE_CHARACTER_FORWARD = 1 << 8,
	MOVE_CHARACTER_LEFT = 1 << 9,
	MOVE_CHARACTER_RIGHT = 1 << 10,
	MOVE_CHARACTER_BACKWARD = 1 << 11,
	MOVE_FORWARD_STOP = 1 << 12,
	MOVE_LEFT_STOP = 1 << 13,
	MOVE_RIGHT_STOP = 1 << 14,
	MOVE_BACKWARD_STOP = 1 << 15,
	MOVE_JUMP = 1 << 16,
	MOVE_CROUCH = 1 << 17,
	CHARACTER_IDLE = 1 << 18,
	CAMERA_ZOOM_IN = 1 << 19,
	CAMERA_ZOOM_OUT = 1 << 20,
	CHARACTER_EAT = 1 << 21,
	CHARACTER_DRINK = 1 << 22,
	END_OF_EVENTS = 1 << 23
};

struct PlayerVar{

	//max health
	int maxHealth;
	int maxHunger;
	int maxThirst;
	int maxStamina;

	//in radians
	float yawLimit;
	float pitchMax;
	float pitchMin;

	//in degrees
	float fov;

	//scalar
	f32 cameraOffsetX;
	f32 cameraOffsetZ;

	float forwardSpeed;
	float backwardSpeed;
	float strafeSpeed;
	float spinSpeed;
	float headSpeed;
	float sprintMultiplier;

	//type
	//0 = herbivore
	//1 = carnivore
	//2 = omnivore
	int type;

	PlayerVar()
		: maxHealth(100)
		, maxHunger(100)
		, maxThirst(100)
		, maxStamina(100)
		, yawLimit(0.5f)
		, pitchMax(0.5f)
		, pitchMin(0.5f)
		, fov(60.0f)
		, cameraOffsetX(0.0f)
		, cameraOffsetZ(1.0f)
		, forwardSpeed(1.0f)
		, backwardSpeed(1.0f)
		, strafeSpeed(1.0f)
		, spinSpeed(1.0f)
		, headSpeed(1.0f)
		, sprintMultiplier(2.0f)
		, type(0)
	{
	}

	void Serialize(Serialization::IArchive& ar)
	{
		ar(maxHealth, "maxHealth", "MaxHealth");
		ar(maxHunger, "maxHunger", "MaxHunger");
		ar(maxThirst, "maxThirst", "MaxThirst");
		ar(maxStamina, "maxStamina", "MaxStamina");
		ar(yawLimit, "yawLimit", "YawLimit");
		ar(pitchMax, "pitchMax", "PitchMax");
		ar(pitchMin, "pitchMin", "PitchMin");
		ar(fov, "fov", "FOV");
		ar(cameraOffsetX, "cameraOffsetX", "CameraOffsetX");
		ar(cameraOffsetZ, "cameraOffsetZ", "CameraOffsetZ");
		ar(forwardSpeed, "fowardSpeed", "forwardSpeed");
		ar(backwardSpeed, "backwardSpeed", "backwardSpeed");
		ar(strafeSpeed, "strafeSpeed", "StrafeSpeed");
		ar(spinSpeed, "spinSpeed", "spinSpeed");
		ar(headSpeed, "headSpeed", "HeadSpeed");
		ar(sprintMultiplier, "sprintMultiplier", "sprintMultiplier");
		ar(type, "type", "Type");
	}
};


